import random
from datetime import datetime

POP = 30;
LEN = 10;
MUT = 0.1;
REC = 0.5;
END = 1000;
SUMTARG = 36.0;
PRODTARG = 360.0;
genes=[]

@profile
def init_pop():
    for x in range(POP):
        genes = [[0 for j in range(LEN)] for i in range(POP)]


    print('passei o primeiro')
    for x in range(POP):
        for y in range(LEN):
            aleatorio = random.random()
            if aleatorio < 0.5:
                genes[x][y]=0
            else:
                genes[x][y]=1

    return genes
                
@profile
def evaluate(n):
    soma = 0
    prod = 1
    for x in range(LEN):
        if genes[n][x] == 0:
            soma += (1 + x)

        else:
            prod *= (1 + x)
    scaled_sum_error = (soma - SUMTARG) / SUMTARG;
    scaled_prod_error = (prod - PRODTARG) / PRODTARG;
    combined_error = abs(scaled_sum_error) + abs(scaled_prod_error)
    return combined_error

@profile
def run():
    for x in range(0,END):
        a = int(POP * random.random())
        b = int(POP * random.random())
        if evaluate(a) < evaluate(b): 
            Winner = a
            Loser = b
        else:
            Winner = b
            Loser = a
        for i in range(0,LEN):
            if random.random() < REC:
                genes[Loser][i] = genes[Winner][i]
            if random.random() < MUT:
                genes[Loser][i] = 1 - genes[Loser][i]
            if (evaluate(Loser) == 0.0):
                display(x, Loser)
                return 0
    return 1

@profile
def display(tournaments, n):
    fim = datetime.now().strftime('%H:%M:%S:%f')
    print("TERMINEI AS: "+ fim)
    print("==============================")
    print("Depois de " + str(tournaments) + " rodadas,a solucao para soma (36), eh: ")
    soma = 0
    for x in range(0,LEN):
        if genes[n][x] == 0:
            print(x + 1)
            soma +=(x+1)
    print("A soma eh: "+str(soma))

    prod = 1
    print("E as cartas para produtos sao: ");
    for x in range(0,LEN):
        if genes[n][x] == 1:
            print(x + 1)
            prod *=(x + 1)
    print("O produto eh: "+str(prod))

inicio = datetime.now().strftime('%H:%M:%S:%f')
print("iniciando algoritmo! "+ inicio)
genes = init_pop()
pop =datetime.now().strftime('%H:%M:%S:%f')
print("populacao iniciada! "+pop)
print("iniciando avaliacao")
resultado = run()

if resultado == 1:
    fim = datetime.now().strftime('%H:%M:%S:%f')
    print("TERMINEI AS: "+ fim)
    print("Solucao nao encontrada em "+str(END)+" vezes.")
