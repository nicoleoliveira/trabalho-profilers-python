# encoding: utf-8
import random
import numpy as np

POP = 30
LEN = 10
MUT = 0.1
REC = 0.5
END = 1000
SUMTARG = 36.0
PRODTARG = 360.0
genes=[]
 

@profile
def init_pop():
    genes = np.random.randint(2, size=(30,10))
    return genes
                
@profile
def evaluate(n):
    (soma, prod) = calcResult(genes[n])
 
    scaled_sum_error = (soma - SUMTARG) / SUMTARG
    scaled_prod_error = (prod - PRODTARG) / PRODTARG
    combined_error = abs(scaled_sum_error) + abs(scaled_prod_error)
    return combined_error

@profile
def run():
    for x in range(0,END):
        a = int(POP * random.random())
        b = int(POP * random.random())
        if evaluate(a) < evaluate(b): 
            Winner = a
            Loser = b
        else:
            Winner = b
            Loser = a
        for i in range(0,LEN):
            if random.randint(0,1) == 0: # recombinação
                genes[Loser][i] = genes[Winner][i] # quem perde, recebe o ganhador
            if random.random() < MUT: # mutação
                genes[Loser][i] = 1 - genes[Loser][i]
            if (evaluate(Loser) == 0.0):
                display(x, SUMTARG, PRODTARG)
                return 0
    return 1

@profile
def calcResult(gene):
    soma = 0
    prod = 1
    for x in range(LEN):
        if gene[x] == 0:
            soma += (1 + x)
        else:
            prod *= (1 + x)
    return (soma, prod)

@profile
def display(tournaments, soma, prod):
    print("Depois de " + str(tournaments) + " rodadas, a solução, é: ")

    print("A soma é: "+str(soma))
    print("O produto é: "+str(prod))
 
genes = init_pop()
resultado = run()

if resultado == 1:
    print("Solução não encontrada em "+str(END)+" vezes.")
