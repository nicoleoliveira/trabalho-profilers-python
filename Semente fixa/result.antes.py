iniciando avaliacao
TERMINEI AS: 10:45:01:814899
==============================
Depois de 191 rodadas,a solucao para soma (36), eh: 
2
7
8
9
10
A soma eh: 36
E as cartas para produtos sao: 
1
3
4
5
6
O produto eh: 360
Wrote profile results to cartasv0001-antes.py.lprof
Timer unit: 1e-06 s

Total time: 0.031714 s
File: cartasv0001-antes.py
Function: evaluate at line 44

Line #      Hits         Time  Per Hit   % Time  Line Contents
==============================================================
    44                                           @profile
    45                                           def evaluate(n):
    46      2298          743      0.3      2.3      soma = 0
    47      2298          737      0.3      2.3      prod = 1
    48     25278         8863      0.4     27.9      for x in range(LEN):
    49     22980         8991      0.4     28.4          if genes[n][x] == 0:
    50     12592         4518      0.4     14.2              soma += (1 + x)
    51                                           
    52                                                   else:
    53     10388         3966      0.4     12.5              prod *= (1 + x)
    54      2298         1085      0.5      3.4      scaled_sum_error = (soma - SUMTARG) / SUMTARG;
    55      2298          995      0.4      3.1      scaled_prod_error = (prod - PRODTARG) / PRODTARG;
    56      2298         1121      0.5      3.5      combined_error = abs(scaled_sum_error) + abs(scaled_prod_error)
    57      2298          695      0.3      2.2      return combined_error

Total time: 0.066459 s
File: cartasv0001-antes.py
Function: run at line 59

Line #      Hits         Time  Per Hit   % Time  Line Contents
==============================================================
    59                                           @profile
    60                                           def run():
    61       192           80      0.4      0.1      for x in range(0,END):
    62       192          153      0.8      0.2          a = int(POP * random.random())
    63       192          120      0.6      0.2          b = int(POP * random.random())
    64       192        10380     54.1     15.6          if evaluate(a) < evaluate(b): 
    65        90           35      0.4      0.1              Winner = a
    66        90           35      0.4      0.1              Loser = b
    67                                                   else:
    68       102           39      0.4      0.1              Winner = b
    69       102           36      0.4      0.1              Loser = a
    70      2105          911      0.4      1.4          for i in range(0,LEN):
    71      1914         1060      0.6      1.6              if random.random() < REC:
    72       952          566      0.6      0.9                  genes[Loser][i] = genes[Winner][i]
    73      1914          937      0.5      1.4              if random.random() < MUT:
    74       213          124      0.6      0.2                  genes[Loser][i] = 1 - genes[Loser][i]
    75      1914        51860     27.1     78.0              if (evaluate(Loser) == 0.0):
    76         1          117    117.0      0.2                  display(x, Loser)
    77         1            6      6.0      0.0                  return 0
    78                                               return 1

Total time: 8.5e-05 s
File: cartasv0001-antes.py
Function: display at line 80

Line #      Hits         Time  Per Hit   % Time  Line Contents
==============================================================
    80                                           @profile
    81                                           def display(tournaments, n):
    82         1           36     36.0     42.4      fim = datetime.now().strftime('%H:%M:%S:%f')
    83         1            4      4.0      4.7      print("TERMINEI AS: "+ fim)
    84         1            1      1.0      1.2      print("==============================")
    85         1            2      2.0      2.4      print("Depois de " + str(tournaments) + " rodadas,a solucao para soma (36), eh: ")
    86         1            0      0.0      0.0      soma = 0
    87        11            5      0.5      5.9      for x in range(0,LEN):
    88        10            5      0.5      5.9          if genes[n][x] == 0:
    89         5            7      1.4      8.2              print(x + 1)
    90         5            1      0.2      1.2              soma +=(x+1)
    91         1            2      2.0      2.4      print("A soma eh: "+str(soma))
    92                                           
    93         1            1      1.0      1.2      prod = 1
    94         1            1      1.0      1.2      print("E as cartas para produtos sao: ");
    95        11            6      0.5      7.1      for x in range(0,LEN):
    96        10            5      0.5      5.9          if genes[n][x] == 1:
    97         5            4      0.8      4.7              print(x + 1)
    98         5            4      0.8      4.7              prod *=(x + 1)
    99         1            1      1.0      1.2      print("O produto eh: "+str(prod))

