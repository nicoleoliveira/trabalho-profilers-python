Depois de 191 rodadas, a solução, é: 
A soma é: 36.0
O produto é: 360.0
Wrote profile results to cartasv0001.py.lprof
Timer unit: 1e-06 s

Total time: 0.033547 s
File: cartasv0001.py
Function: evaluate at line 45

Line #      Hits         Time  Per Hit   % Time  Line Contents
==============================================================
    45                                           @profile
    46                                           def evaluate(n):
    47      2298          768      0.3      2.3      soma = 0
    48      2298          770      0.3      2.3      prod = 1
    49     25278         9415      0.4     28.1      for x in range(LEN):
    50     22980         9562      0.4     28.5          if genes[n][x] == 0:
    51     12592         4675      0.4     13.9              soma += (1 + x)
    52                                                   else:
    53     10388         4136      0.4     12.3              prod *= (1 + x)
    54                                            
    55      2298         1179      0.5      3.5      scaled_sum_error = (soma - SUMTARG) / SUMTARG
    56      2298         1047      0.5      3.1      scaled_prod_error = (prod - PRODTARG) / PRODTARG
    57      2298         1257      0.5      3.7      combined_error = abs(scaled_sum_error) + abs(scaled_prod_error)
    58      2298          738      0.3      2.2      return combined_error

Total time: 0.07028 s
File: cartasv0001.py
Function: run at line 60

Line #      Hits         Time  Per Hit   % Time  Line Contents
==============================================================
    60                                           @profile
    61                                           def run():
    62       192           93      0.5      0.1      for x in range(0,END):
    63       192          206      1.1      0.3          Winner = int(POP * random.random())
    64       192          137      0.7      0.2          Loser = int(POP * random.random())
    65                                           
    66       192        10994     57.3     15.6          if evaluate(Winner) > evaluate(Loser): 
    67        96           42      0.4      0.1              Winner, Loser = Loser, Winner
    68                                           
    69      2105          975      0.5      1.4          for i in range(0,LEN):
    70      1914         1104      0.6      1.6              if random.random() < REC: # recombinação
    71       952          607      0.6      0.9                  genes[Loser][i] = genes[Winner][i] # quem perde, recebe o ganhador
    72      1914          964      0.5      1.4              if random.random() < MUT: # mutação
    73       213          128      0.6      0.2                  genes[Loser][i] = 1 - genes[Loser][i]
    74      1914        54986     28.7     78.2              if (evaluate(Loser) == 0.0):
    75         1           36     36.0      0.1                  display(x, SUMTARG, PRODTARG)
    76         1            8      8.0      0.0                  return 0
    77                                               return 1

Total time: 2.6e-05 s
File: cartasv0001.py
Function: display at line 79

Line #      Hits         Time  Per Hit   % Time  Line Contents
==============================================================
    79                                           @profile
    80                                           def display(tournaments, soma, prod):
    81         1           20     20.0     76.9      print("Depois de " + str(tournaments) + " rodadas, a solução, é: ")
    82                                           
    83         1            5      5.0     19.2      print("A soma é: "+str(soma))
    84         1            1      1.0      3.8      print("O produto é: "+str(prod))

