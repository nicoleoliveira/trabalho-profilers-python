Depois de 191 rodadas, a solução, é: 
A soma é: 36.0
O produto é: 360.0
Filename: cartasv0001.py

Line #    Mem usage    Increment   Line Contents
================================================
    46   28.910 MiB    0.000 MiB   @profile
    47                             def evaluate(n):
    48   28.910 MiB    0.000 MiB       soma = 0
    49   28.910 MiB    0.000 MiB       prod = 1
    50   28.910 MiB    0.000 MiB       for x in range(LEN):
    51   28.910 MiB    0.000 MiB           if genes[n][x] == 0:
    52   28.910 MiB    0.000 MiB               soma += (1 + x)
    53                                     else:
    54   28.910 MiB    0.000 MiB               prod *= (1 + x)
    55                              
    56   28.910 MiB    0.000 MiB       scaled_sum_error = (soma - SUMTARG) / SUMTARG
    57   28.910 MiB    0.000 MiB       scaled_prod_error = (prod - PRODTARG) / PRODTARG
    58   28.910 MiB    0.000 MiB       combined_error = abs(scaled_sum_error) + abs(scaled_prod_error)
    59   28.910 MiB    0.000 MiB       return combined_error


Filename: cartasv0001.py

Line #    Mem usage    Increment   Line Contents
================================================
    61   28.910 MiB    0.000 MiB   @profile
    62                             def run():
    63   28.910 MiB    0.000 MiB       for x in range(0,END):
    64   28.910 MiB    0.000 MiB           Winner = int(POP * random.random())
    65   28.910 MiB    0.000 MiB           Loser = int(POP * random.random())
    66                             
    67   28.910 MiB    0.000 MiB           if evaluate(Winner) > evaluate(Loser): 
    68   28.910 MiB    0.000 MiB               Winner, Loser = Loser, Winner
    69                             
    70   28.910 MiB    0.000 MiB           for i in range(0,LEN):
