iniciando avaliacao
TERMINEI AS: 11:21:53:592077
==============================
Depois de 191 rodadas,a solucao para soma (36), eh: 
2
7
8
9
10
A soma eh: 36
E as cartas para produtos sao: 
1
3
4
5
6
O produto eh: 360
Filename: cartasv0001-antes.py

Line #    Mem usage    Increment   Line Contents
================================================
    45   20.441 MiB    0.000 MiB   @profile
    46                             def evaluate(n):
    47   20.441 MiB    0.000 MiB       soma = 0
    48   20.441 MiB    0.000 MiB       prod = 1
    49   20.441 MiB    0.000 MiB       for x in range(LEN):
    50   20.441 MiB    0.000 MiB           if genes[n][x] == 0:
    51   20.441 MiB    0.000 MiB               soma += (1 + x)
    52                             
    53                                     else:
    54   20.441 MiB    0.000 MiB               prod *= (1 + x)
    55   20.441 MiB    0.000 MiB       scaled_sum_error = (soma - SUMTARG) / SUMTARG;
    56   20.441 MiB    0.000 MiB       scaled_prod_error = (prod - PRODTARG) / PRODTARG;
    57   20.441 MiB    0.000 MiB       combined_error = abs(scaled_sum_error) + abs(scaled_prod_error)
    58   20.441 MiB    0.000 MiB       return combined_error


Filename: cartasv0001-antes.py

Line #    Mem usage    Increment   Line Contents
================================================
    60   20.441 MiB    0.000 MiB   @profile
    61                             def run():
    62   20.441 MiB    0.000 MiB       for x in range(0,END):
    63   20.441 MiB    0.000 MiB           a = int(POP * random.random())
    64   20.441 MiB    0.000 MiB           b = int(POP * random.random())
    65   20.441 MiB    0.000 MiB           if evaluate(a) < evaluate(b): 
    66   20.441 MiB    0.000 MiB               Winner = a
    67   20.441 MiB    0.000 MiB               Loser = b
    68                                     else:
    69   20.441 MiB    0.000 MiB               Winner = b
    70   20.441 MiB    0.000 MiB               Loser = a
    71   20.441 MiB    0.000 MiB           for i in range(0,LEN):
    72   20.441 MiB    0.000 MiB               if random.random() < REC:
    73   20.441 MiB    0.000 MiB                   genes[Loser][i] = genes[Winner][i]
    74   20.441 MiB    0.000 MiB               if random.random() < MUT:
    75   20.441 MiB    0.000 MiB                   genes[Loser][i] = 1 - genes[Loser][i]
    76   20.441 MiB    0.000 MiB               if (evaluate(Loser) == 0.0):
    77   20.441 MiB    0.000 MiB                   display(x, Loser)
    78   20.441 MiB    0.000 MiB                   return 0
    79                                 return 1


Filename: cartasv0001-antes.py

Line #    Mem usage    Increment   Line Contents
================================================
    81   20.441 MiB    0.000 MiB   @profile
    82                             def display(tournaments, n):
    83   20.441 MiB    0.000 MiB       fim = datetime.now().strftime('%H:%M:%S:%f')
    84   20.441 MiB    0.000 MiB       print("TERMINEI AS: "+ fim)
    85   20.441 MiB    0.000 MiB       print("==============================")
    86   20.441 MiB    0.000 MiB       print("Depois de " + str(tournaments) + " rodadas,a solucao para soma (36), eh: ")
    87   20.441 MiB    0.000 MiB       soma = 0
    88   20.441 MiB    0.000 MiB       for x in range(0,LEN):
    89   20.441 MiB    0.000 MiB           if genes[n][x] == 0:
    90   20.441 MiB    0.000 MiB               print(x + 1)
    91   20.441 MiB    0.000 MiB               soma +=(x+1)
    92   20.441 MiB    0.000 MiB       print("A soma eh: "+str(soma))
    93                             
    94   20.441 MiB    0.000 MiB       prod = 1
    95   20.441 MiB    0.000 MiB       print("E as cartas para produtos sao: ");
    96   20.441 MiB    0.000 MiB       for x in range(0,LEN):
    97   20.441 MiB    0.000 MiB           if genes[n][x] == 1:
    98   20.441 MiB    0.000 MiB               print(x + 1)
    99   20.441 MiB    0.000 MiB               prod *=(x + 1)
   100   20.441 MiB    0.000 MiB       print("O produto eh: "+str(prod))


