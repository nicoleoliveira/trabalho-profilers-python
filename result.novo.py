Depois de 330 rodadas, a solução, é: 
A soma é: 36.0
O produto é: 360.0
Wrote profile results to cartasv0001.py.lprof
Timer unit: 1e-06 s

Total time: 2e-05 s
File: cartasv0001.py
Function: init_pop at line 45

Line #      Hits         Time  Per Hit   % Time  Line Contents
==============================================================
    45                                           @profile
    46                                           def init_pop():
    47         1           19     19.0     95.0      genes = np.random.randint(2, size=(30,10))
    48         1            1      1.0      5.0      return genesFixo

Total time: 0.08768 s
File: cartasv0001.py
Function: evaluate at line 50

Line #      Hits         Time  Per Hit   % Time  Line Contents
==============================================================
    50                                           @profile
    51                                           def evaluate(n):
    52      3966        81226     20.5     92.6      (soma, prod) = calcResult(genes[n])
    53                                            
    54      3966         1806      0.5      2.1      scaled_sum_error = (soma - SUMTARG) / SUMTARG
    55      3966         1534      0.4      1.7      scaled_prod_error = (prod - PRODTARG) / PRODTARG
    56      3966         1919      0.5      2.2      combined_error = abs(scaled_sum_error) + abs(scaled_prod_error)
    57      3966         1195      0.3      1.4      return combined_error

Total time: 0.118494 s
File: cartasv0001.py
Function: run at line 59

Line #      Hits         Time  Per Hit   % Time  Line Contents
==============================================================
    59                                           @profile
    60                                           def run():
    61       331          139      0.4      0.1      for x in range(0,END):
    62       331          254      0.8      0.2          a = int(POP * random.random())
    63       331          221      0.7      0.2          b = int(POP * random.random())
    64       331        16705     50.5     14.1          if evaluate(a) < evaluate(b): 
    65       163           70      0.4      0.1              Winner = a
    66       163           67      0.4      0.1              Loser = b
    67                                                   else:
    68       168           74      0.4      0.1              Winner = b
    69       168           75      0.4      0.1              Loser = a
    70      3634         1730      0.5      1.5          for i in range(0,LEN):
    71      3304        11808      3.6     10.0              if random.randint(0,1) == 0: # recombinação
    72      1703         1027      0.6      0.9                  genes[Loser][i] = genes[Winner][i] # quem perde, recebe o ganhador
    73      3304         1787      0.5      1.5              if random.random() < MUT: # mutação
    74       340          219      0.6      0.2                  genes[Loser][i] = 1 - genes[Loser][i]
    75      3304        84289     25.5     71.1              if (evaluate(Loser) == 0.0):
    76         1           24     24.0      0.0                  display(x, SUMTARG, PRODTARG)
    77         1            5      5.0      0.0                  return 0
    78                                               return 1

Total time: 0.04192 s
File: cartasv0001.py
Function: calcResult at line 80

Line #      Hits         Time  Per Hit   % Time  Line Contents
==============================================================
    80                                           @profile
    81                                           def calcResult(gene):
    82      3966         1057      0.3      2.5      soma = 0
    83      3966         1111      0.3      2.7      prod = 1
    84     43626        13196      0.3     31.5      for x in range(LEN):
    85     39660        12385      0.3     29.5          if gene[x] == 0:
    86     21867         6958      0.3     16.6              soma += (1 + x)
    87                                                   else:
    88     17793         6023      0.3     14.4              prod *= (1 + x)
    89      3966         1190      0.3      2.8      return (soma, prod)

Total time: 1.9e-05 s
File: cartasv0001.py
Function: display at line 91

Line #      Hits         Time  Per Hit   % Time  Line Contents
==============================================================
    91                                           @profile
    92                                           def display(tournaments, soma, prod):
    93         1           13     13.0     68.4      print("Depois de " + str(tournaments) + " rodadas, a solução, é: ")
    94                                           
    95         1            4      4.0     21.1      print("A soma é: "+str(soma))
    96         1            2      2.0     10.5      print("O produto é: "+str(prod))

