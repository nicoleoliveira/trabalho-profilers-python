iniciando algoritmo! 10:34:22:968467
populacao iniciada! 10:34:22:968506
iniciando avaliacao
TERMINEI AS: 10:34:23:329813
Solucao nao encontrada em 1000 vezes.
Wrote profile results to cartasv0001-antes.py.lprof
Timer unit: 1e-06 s

Total time: 3e-06 s
File: cartasv0001-antes.py
Function: init_pop at line 44

Line #      Hits         Time  Per Hit   % Time  Line Contents
==============================================================
    44                                           @profile
    45                                           def init_pop():
    46                                               # for x in range(POP):
    47                                               #     genes = [[0 for j in range(LEN)] for i in range(POP)]
    48                                           
    49                                           
    50                                               # print('passei o primeiro')
    51                                               # for x in range(POP):
    52                                               #     for y in range(LEN):
    53                                               #         aleatorio = random.random()
    54                                               #         if aleatorio < 0.5:
    55                                               #             genes[x][y]=0
    56                                               #         else:
    57                                               #             genes[x][y]=1
    58         1            2      2.0     66.7      genes = genesFixo
    59                                           
    60         1            1      1.0     33.3      return genes

Total time: 0.162476 s
File: cartasv0001-antes.py
Function: evaluate at line 62

Line #      Hits         Time  Per Hit   % Time  Line Contents
==============================================================
    62                                           @profile
    63                                           def evaluate(n):
    64     12000         3842      0.3      2.4      soma = 0
    65     12000         3866      0.3      2.4      prod = 1
    66    132000        44709      0.3     27.5      for x in range(LEN):
    67    120000        45620      0.4     28.1          if genes[n][x] == 0:
    68     63214        22825      0.4     14.0              soma += (1 + x)
    69                                           
    70                                                   else:
    71     56786        21793      0.4     13.4              prod *= (1 + x)
    72     12000         5396      0.4      3.3      scaled_sum_error = (soma - SUMTARG) / SUMTARG;
    73     12000         5138      0.4      3.2      scaled_prod_error = (prod - PRODTARG) / PRODTARG;
    74     12000         5738      0.5      3.5      combined_error = abs(scaled_sum_error) + abs(scaled_prod_error)
    75     12000         3549      0.3      2.2      return combined_error

Total time: 0.339418 s
File: cartasv0001-antes.py
Function: run at line 77

Line #      Hits         Time  Per Hit   % Time  Line Contents
==============================================================
    77                                           @profile
    78                                           def run():
    79      1001          410      0.4      0.1      for x in range(0,END):
    80      1000          741      0.7      0.2          a = int(POP * random.random())
    81      1000          646      0.6      0.2          b = int(POP * random.random())
    82      1000        52821     52.8     15.6          if evaluate(a) < evaluate(b): 
    83       464          189      0.4      0.1              Winner = a
    84       464          178      0.4      0.1              Loser = b
    85                                                   else:
    86       536          208      0.4      0.1              Winner = b
    87       536          196      0.4      0.1              Loser = a
    88     11000         4718      0.4      1.4          for i in range(0,LEN):
    89     10000         5270      0.5      1.6              if random.random() < REC:
    90      4932         2731      0.6      0.8                  genes[Loser][i] = genes[Winner][i]
    91     10000         4914      0.5      1.4              if random.random() < MUT:
    92       963          556      0.6      0.2                  genes[Loser][i] = 1 - genes[Loser][i]
    93     10000       265840     26.6     78.3              if (evaluate(Loser) == 0.0):
    94                                                           display(x, Loser)
    95                                                           return 0
    96         1            0      0.0      0.0      return 1

Total time: 0 s
File: cartasv0001-antes.py
Function: display at line 98

Line #      Hits         Time  Per Hit   % Time  Line Contents
==============================================================
    98                                           @profile
    99                                           def display(tournaments, n):
   100                                               fim = datetime.now().strftime('%H:%M:%S:%f')
   101                                               print("TERMINEI AS: "+ fim)
   102                                               print("==============================")
   103                                               print("Depois de " + str(tournaments) + " rodadas,a solucao para soma (36), eh: ")
   104                                               soma = 0
   105                                               for x in range(0,LEN):
   106                                                   if genes[n][x] == 0:
   107                                                       print(x + 1)
   108                                                       soma +=(x+1)
   109                                               print("A soma eh: "+str(soma))
   110                                           
   111                                               prod = 1
   112                                               print("E as cartas para produtos sao: ");
   113                                               for x in range(0,LEN):
   114                                                   if genes[n][x] == 1:
   115                                                       print(x + 1)
   116                                                       prod *=(x + 1)
   117                                               print("O produto eh: "+str(prod))

