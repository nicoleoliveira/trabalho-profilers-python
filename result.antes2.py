iniciando algoritmo! 09:21:11:168535
passei o primeiro
populacao iniciada! 09:21:11:176972
iniciando avaliacao
TERMINEI AS: 09:21:11:361519
==============================
Depois de 533 rodadas,a solucao para soma (36), eh: 
2
7
8
9
10
A soma eh: 36
E as cartas para produtos sao: 
1
3
4
5
6
O produto eh: 360
Wrote profile results to cartasv0001-antes.py.lprof
Timer unit: 1e-06 s

Total time: 0.004473 s
File: cartasv0001-antes.py
Function: init_pop at line 13

Line #      Hits         Time  Per Hit   % Time  Line Contents
==============================================================
    13                                           @profile
    14                                           def init_pop():
    15        31           12      0.4      0.3      for x in range(POP):
    16      9930         3952      0.4     88.4          genes = [[0 for j in range(LEN)] for i in range(POP)]
    17                                           
    18                                           
    19         1            1      1.0      0.0      print('passei o primeiro')
    20        31           11      0.4      0.2      for x in range(POP):
    21       330          139      0.4      3.1          for y in range(LEN):
    22       300          123      0.4      2.7              aleatorio = random.random()
    23       300          110      0.4      2.5              if aleatorio < 0.5:
    24       152           67      0.4      1.5                  genes[x][y]=0
    25                                                       else:
    26       148           58      0.4      1.3                  genes[x][y]=1
    27                                           
    28         1            0      0.0      0.0      return genes

Total time: 0.083852 s
File: cartasv0001-antes.py
Function: evaluate at line 30

Line #      Hits         Time  Per Hit   % Time  Line Contents
==============================================================
    30                                           @profile
    31                                           def evaluate(n):
    32      6402         2006      0.3      2.4      soma = 0
    33      6402         1944      0.3      2.3      prod = 1
    34     70422        23299      0.3     27.8      for x in range(LEN):
    35     64020        23473      0.4     28.0          if genes[n][x] == 0:
    36     36499        13052      0.4     15.6              soma += (1 + x)
    37                                           
    38                                                   else:
    39     27521        10108      0.4     12.1              prod *= (1 + x)
    40      6402         2644      0.4      3.2      scaled_sum_error = (soma - SUMTARG) / SUMTARG;
    41      6402         2569      0.4      3.1      scaled_prod_error = (prod - PRODTARG) / PRODTARG;
    42      6402         2869      0.4      3.4      combined_error = abs(scaled_sum_error) + abs(scaled_prod_error)
    43      6402         1888      0.3      2.3      return combined_error

Total time: 0.174673 s
File: cartasv0001-antes.py
Function: run at line 45

Line #      Hits         Time  Per Hit   % Time  Line Contents
==============================================================
    45                                           @profile
    46                                           def run():
    47       534          191      0.4      0.1      for x in range(0,END):
    48       534          386      0.7      0.2          a = int(POP * random.random())
    49       534          348      0.7      0.2          b = int(POP * random.random())
    50       534        27245     51.0     15.6          if evaluate(a) < evaluate(b): 
    51       278           94      0.3      0.1              Winner = a
    52       278          111      0.4      0.1              Loser = b
    53                                                   else:
    54       256           93      0.4      0.1              Winner = b
    55       256          108      0.4      0.1              Loser = a
    56      5867         2310      0.4      1.3          for i in range(0,LEN):
    57      5334         2580      0.5      1.5              if random.random() < REC:
    58      2609         1336      0.5      0.8                  genes[Loser][i] = genes[Winner][i]
    59      5334         2446      0.5      1.4              if random.random() < MUT:
    60       509          261      0.5      0.1                  genes[Loser][i] = 1 - genes[Loser][i]
    61      5334       137078     25.7     78.5              if (evaluate(Loser) == 0.0):
    62         1           83     83.0      0.0                  display(x, Loser)
    63         1            3      3.0      0.0                  return 0
    64                                               return 1

Total time: 5.2e-05 s
File: cartasv0001-antes.py
Function: display at line 66

Line #      Hits         Time  Per Hit   % Time  Line Contents
==============================================================
    66                                           @profile
    67                                           def display(tournaments, n):
    68         1           13     13.0     25.0      fim = datetime.now().strftime('%H:%M:%S:%f')
    69         1            2      2.0      3.8      print("TERMINEI AS: "+ fim)
    70         1            0      0.0      0.0      print("==============================")
    71         1            2      2.0      3.8      print("Depois de " + str(tournaments) + " rodadas,a solucao para soma (36), eh: ")
    72         1            0      0.0      0.0      soma = 0
    73        11            5      0.5      9.6      for x in range(0,LEN):
    74        10            5      0.5      9.6          if genes[n][x] == 0:
    75         5            6      1.2     11.5              print(x + 1)
    76         5            3      0.6      5.8              soma +=(x+1)
    77         1            1      1.0      1.9      print("A soma eh: "+str(soma))
    78                                           
    79         1            0      0.0      0.0      prod = 1
    80         1            1      1.0      1.9      print("E as cartas para produtos sao: ");
    81        11            4      0.4      7.7      for x in range(0,LEN):
    82        10            4      0.4      7.7          if genes[n][x] == 1:
    83         5            3      0.6      5.8              print(x + 1)
    84         5            2      0.4      3.8              prod *=(x + 1)
    85         1            1      1.0      1.9      print("O produto eh: "+str(prod))

